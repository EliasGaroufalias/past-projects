from graphics import *
import random


class Snake():

	pos = [0,0]
	size = 1

	def __init__(self, resolution):
		self.pos = [random.randint(0,resolution),random.randint(0,resolution)]
		self.size = 1

	def move(self, direction):
		#print ' ',(direction//2+1)%2,direction//2, ((-1)**((direction+1)%2)),
		self.pos[0] += (direction//2+1)%2*((-1)**((direction+1)%2))
		self.pos[1] += direction//2*((-1)**((direction+1)%2))


def main():
	size = 800
	resolution = 32
	tile_size = size//resolution
	size = tile_size*resolution

	snek = Snake(resolution)
	print snek.pos
	snek.move(0)
	print snek.pos
	snek.move(1)
	print snek.pos
	snek.move(2)
	print snek.pos
	snek.move(3)
	print snek.pos

	win = GraphWin('Snake Game', size, size)
	win.setBackground(color_rgb(0,0,0))

	tiling = []
	tiles = []
	for i in xrange(resolution):
		for j in xrange(resolution):
			tiles.append(Rectangle(Point(tile_size*i,tile_size*j),Point(tile_size*(i+1),tile_size*(j+1))))
		tiling.append(list(tiles))
		tiles = []
	for j in tiling:
		for i in j:
			i.setFill(color_rgb(0,0,0)) 
			i.setOutline(color_rgb(150,150,150))
			i.draw(win)
	tiling[snek.pos[0]][snek.pos[1]].setFill(color_rgb(255,255,255))

	win.getMouse()
	win.close()

main()