def MnC(IS, FS):
	closed = []
	frontier = []
	evaluatedIS = h(IS)
	frontier.append(evaluatedIS)
	while current != FS:
		current = Best(frontier)
		frontier.remove(current)
		if current not in closed:
			children = Expand(current)
			for child in children:
				frontier.append(h(child))
			closed.append(current)
		if len(frontier) == 0:
			return False
	return True


'''
The states are represented by the # of missionaries and cannibals, and the position of the boat
on the first side, starting with 3,3 on that side and ending with 0,0 on it.
State[0] = missionaries, State[1] = cannibals, State[2] = boat position (0=start, 1=end)
'''
def Expand(state):
	children = []
# going forward
	if state[2] == 0:
		if state[0] > 1 and (state[0] > state[1] + 1 or state[0] == 2):
			children.append([state[0]-2,state[1]]  ,(state[2]+1)%2)
		if state[0] > 0 and state[1] > 0:
			children.append([state[0]-1,state[1]-1],(state[2]+1)%2)
		if state[1] > 1 and 3 - state[0] > 3 - state[1] + 1:
			children.append([state[0]  ,state[1]-2],(state[2]+1)%2)
# going backwards
	if state[2] == 1:
		if 3 - state[0] > 1 and (3 - state[0] > 3 - state[1] + 1 or state[0] == 1):
			children.append([state[0]+2,state[1]]  ,(state[2]+1)%2)
		if 3 - state[0] > 0 and 3 - state[1] > 0:
			children.append([state[0]+1,state[1]+1],(state[2]+1)%2)
		if 3 - state[1] > 1 and state[0] > state[1] + 1:
			children.append([state[0]  ,state[1]+2],(state[2]+1)%2)
	return children


h(state):
	return [state[0], state[1], state[2], state[0] + state[1]]


Best(list1):
	#find list


MnC([3,3,0], [0,0,1])