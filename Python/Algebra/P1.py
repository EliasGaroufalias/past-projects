try:
	met = raw_input("\nDwse metathesh:\n\n\t")
	met = [int(i) for i in met.split(' ')]
except:
	print '\nWrong Input.'	
	exit()
for i in met:
	if i > len(met) or i < 1 :
		print '\nWrong Input.'
		exit()

print '\nTo mhkos ths metatheshs einai', len(met)

metath = {}
metath_ = {}
for i in xrange(1, len(met) + 1):
	metath[i] = met[i-1]
	metath_[met[i-1]] = i
antistrofh = []
for i in xrange(1, len(met) + 1):
	antistrofh.append(metath_[i])
print "Antistrofh metathesh:", 
for i in xrange(len(antistrofh)):
	print antistrofh[i],

check = [i+1 for i in xrange(len(met))]
j=0
gin_kyklwn = []
kyklos = []
while sum(check) != 0:
	if check[j] == 0:
		if len(kyklos) > 1:
			gin_kyklwn.append(kyklos)
		kyklos = []
	while check[j] == 0:
		j = (j + 1) % len(met)
	check[j] = 0
	j = metath[j+1] - 1
	kyklos.append(j+1)
if len(kyklos) > 1:
	gin_kyklwn.append(kyklos)
print "\nWs ginomeno kyklwn grafetai:", 
for i in xrange(len(gin_kyklwn)):
	print '(',
	for j in xrange(len(gin_kyklwn[i])):
		print gin_kyklwn[i][j],
	print ')', chr(248)*(i<len(gin_kyklwn)-1),

gin_ant = []
for i in xrange(len(gin_kyklwn)):
	if len(gin_kyklwn[i]) > 1:
		for j in range(1, len(gin_kyklwn[i])):
			gin_ant.append([gin_kyklwn[i][0], gin_kyklwn[i][j]])
print "\nWs ginomeno antimetathesewn grafetai:", 
for i in xrange(len(gin_ant)):
	print '(',
	for j in xrange(len(gin_ant[i])):
		print gin_ant[i][j],
	print ')', chr(248)*(i<len(gin_ant)-1),

print '\nParabaseis:',
for i in xrange(1,len(met)+1):
	for j in xrange(i + 1, len(met) + 1):
		if metath[i] > metath[j]:
			print '('+str(i)+','+str(j)+')',

print '\nH metathesh einai artia.'*((len(gin_ant)+1)%2) + '\nH metathesh einai peritth.'*(len(gin_ant)%2)