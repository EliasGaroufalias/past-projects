dim = []
try:
	temp = raw_input("Give me the dimensions of the #1 matrix (separated by space)\n\t").split(' ')
	if len(temp) > 2:
		raise
	for i in xrange(2):
		dim.append(int(temp[i]))
	for i in xrange(2,6):
		temp = raw_input("Give me the dimensions of the #" + str(i) + " matrix:\n\t").split(' ')
		if (len(temp) > 2) or (int(temp[0]) != dim[-1]):
			raise
		dim.append(int(temp[1]))
	print ' '
except:
	print 'Wrong Input'
	exit()

sum0 = 0
while len(dim) > 2:
	if len(dim) > 3:
		maxv = dim[1]
		maxi = 1
		for i in xrange(1, len(dim) - 1):
			if dim[i] > maxv:
				maxv = dim[i]
				maxi = i
	else:
		maxi = 1
	sum0 += dim[maxi-1]*dim[maxi]*dim[maxi+1]
	dim.pop(maxi)
print 'To elaxisto plhthos pollaplasiasmwn gi aytes tis mhtres einai',sum0