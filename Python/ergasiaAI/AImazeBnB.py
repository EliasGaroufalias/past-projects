def ListToString(l1):
	stringList = ""
	for i in l1:
		stringList += '\n' + str(i)
	return stringList


def PathToList(path):
	mazePrnt = [([0]*len(maze[0])) for i in range(len(maze))]
	order = 1
	for i in path:
		mazePrnt[i[0]][i[1]] = order
		order += 1
	return mazePrnt


def BuildNextPoint(path, exclusions, maxcost):
	choiceness = 0
	point = False
	i = path[-1][0]
	j = path[-1][1]
	if i < (len(maze)-1) and maze[i+1][j] == 1 and [i+1,j] not in path:
		if exclusions == 0:
			if point == False:
				point = [i+1,j]
			else:
				choiceness += 1
		else:
			exclusions -= 1
	if i > 0 and maze[i-1][j] == 1 and [i-1,j] not in path:
		if exclusions == 0:
			if point == False:
				point = [i-1,j]
			else:
				choiceness += 1
		else:
			exclusions -= 1
	if j < (len(maze[0])-1) and maze[i][j+1] == 1 and [i,j+1] not in path:
		if exclusions == 0:
			if point == False:
				point = [i,j+1]
			else:
				choiceness += 1
		else:
			exclusions -= 1
	if j > 0 and maze[i][j-1] == 1 and [i,j-1] not in path:
		if exclusions == 0:
			if point == False:
				point = [i,j-1]
			else:
				choiceness += 1
		else:
			exclusions -= 1
	if choiceness > 0 and len(path) < maxcost:
		for k in range(1, choiceness+1):
			choices.append([[i,j], k])
	return point


def BuildNewPath(path, exclusions, maxcost):
	nextPoint = BuildNextPoint(path, exclusions, maxcost)
	while len(path) < maxcost and path[-1] != exitPoint:
		nextPoint = BuildNextPoint(path, 0, maxcost)
		if nextPoint == False:
			break
		else:
			path.append(nextPoint)
	return path


def FindPathBnB():
	paths = []
	allpaths = []
	cost = float('inf')
	bestPath = "No Paths Found"
	paths.append(BuildNewPath([entryPoint], 0, cost))
	while True:
		lastPath = True
		pathLen = len(paths[-1])
		path = ListToString(PathToList(paths[-1]))
		if cost > pathLen and paths[-1][-1] == exitPoint:
			cost = pathLen
			bestPath = path
		if paths[-1][-1] == exitPoint:
			path += "\t(Found the Exit)"
		allpaths.append(path)
		if len(choices) > 0:
			for i in range(pathLen, 0, -1):
				if paths[-1][-1] == choices[-1][0]:
					choice = choices.pop()
					paths.append(BuildNewPath(paths[-1], choice[1], cost))
					lastPath = False
					break
				else:
					paths[-1].pop()
		if lastPath:
			break
	return [bestPath, allpaths]


choices = []

maze = [[0,0,0,0,0],
		[0,1,1,1,0],
		[0,1,0,1,1],
		[1,1,1,1,0],
		[0,0,0,0,0]]
entryPoint = [3,0]
exitPoint = [2,4]

pathFound = FindPathBnB()

print("\nThe fastest path is:\n" + pathFound[0])

answer = input("\nPrint all paths found? (Y/N)\n\t")
while answer not in "YyNn":
	print ("I couldn\'t understand, please answer with \"Y\" or \"N\"")
if answer in "Yy":
	for path in pathFound[1]:
		print(path)