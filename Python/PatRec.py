import numpy as np
import sqlite3

#names = list(map(lambda x: x[0], c.description))
#for i in names:
#	print i

def classify(match, res, classifier, output):
#	output_dict = {0: 'H', 1: 'D', 2: 'A'}
	if (np.matmul(classifier[k], np.transpose(match)).item(0) > 0) and output == res:
		return 1	
	elif (np.matmul(classifier[k], np.transpose(match)).item(0) < 0) and output != res:
		return 1
	else:
		return 0


def load_classifiers():
	f = open("classifier.txt", "r")
	classifiers = []
	for classif in f.read().split('\n---\n'):
		classifier = []
		for line in classif.split('\n'):
			classifier_line = line.strip('][').split(', ')
			for i in xrange(len(classifier_line)):
				classifier_line[i] = float(classifier_line[i])
			classifier.append(classifier_line)
		classifier = np.matrix(classifier)
		classifiers.append(classifier)
	f.close()
	return classifiers


def train_classifier_LS(X, Y):
	classifiers = []
	for output in xrange(3):
		classifier = []
		for k in xrange(4):
			mul0 = np.matmul(X[k], np.transpose(X[k]))
			mul1 = np.linalg.inv(mul0)
			mul2 = np.matmul(np.transpose(X[k]),np.transpose(Y[output]))
			newlin = np.matmul(mul1, mul2)
			classifier.append(newlin)
		classifiers.append(classifier)
	f = open("classifier.txt", "w")
	for i in classifiers:
		f.write(i)
	return classifiers


conn = sqlite3.connect('database.sqlite')
c = conn.cursor()

c.execute("SELECT home_team_api_id, away_team_api_id, home_team_goal, away_team_goal, B365H, B365D, B365A, BWH, BWD, BWA, IWH, IWD, IWA, LBH, LBD, LBA FROM Match")
Matches = c.fetchall()
M = []
for Match in Matches:
	if Match[2] > Match[3]:
		M.append([Match[0], Match[1], 0])
	elif Match[2] < Match[3]:
		M.append([Match[0], Match[1], 1])
	else:
		M.append([Match[0], Match[1], 2])

psi = []
for k in xrange(4):
	company_psi = []
	for Match in Matches:
		if (Match[4+3*k] is not None) and (Match[5+3*k] is not None) and (Match[6+3*k] is not None):
			company_psi.append([Match[4+3*k], Match[5+3*k], Match[6+3*k], 1])
	if not (company_psi == []):
		company_psi = np.matrix(company_psi)
		psi.append(company_psi)


c.execute("SELECT team_api_id, buildUpPlaySpeed, buildUpPlayPassing, chanceCreationPassing, chanceCreationCrossing, chanceCreationShooting, defencePressure, defenceAggression, defenceTeamWidth FROM Team_Attributes")
fi = c.fetchall()
fi = np.matrix(fi)

conn.commit()
conn.close()


Y = []
for output in xrange(3):
	output_Y = []
	for i in M:
		if i[2] == output:
			output_Y.append(1)
		else:
			output_Y.append(0)
	output_Y = np.matrix(output_Y)
	Y.append(output_Y)
classifiers = train_classifier_LS(psi, Y)

companies_dict = {0: 'B365', 1: 'BW', 2: 'IW', 3: 'LB'}
results = []
for k in xrange(4):
	company_accuracy = 0
	for output in xrange(3):
		for i in xrange(len(psi[k])):
			company_accuracy += classify(psi[k][i], M[i][2], classifiers[output], output)
	results.append(company_accuracy/(3*float(len(psi[k]))))


max_company_accuracy = max(results)
for i in xrange(len(results)):
	print "Company \"" + companies_dict[i] + "\" has a " + str(results[i]) + "% accuracy."
for i in xrange(len(results)):
	if results[i] == max_company_accuracy:
		print "\nCompany \"" + companies_dict[i] + "\" is the most accurate."