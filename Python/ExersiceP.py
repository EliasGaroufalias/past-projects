from sage.all import *
import binascii
import base64
import json
import ast

def en_to_base64(data):
	encodedBytes = base64.b64encode(data.encode("utf-8"))
	encodedStr = str(encodedBytes)
	return encodedStr


def writeToJSONFile(path, fileName, data):
    filePathNameWExt = './' + path + '/' + fileName + '.json'
    with open(filePathNameWExt, 'w') as fp:
        json.dump(data, fp)


f = raw_input("give file name(.txt): \n\t")
my_string = open(f, 'r').read()
my_matrix = raw_input("Give me a matrix in python list format. Each cell must have 0 or 1 in it.\nThis will be used as the Generator matrix for the linear code:\n\t")
matrix1 = ast.literal_eval(my_matrix);
err = raw_input("Give me the number of errors:\n\t")

letters = []
only_letters = []
for letter in my_string:
	if letter not in letters:
		freq = my_string.count(letter)
		letters.append(freq)
		letters.append(letter)
		only_letters.append(letter)

nodes = []
while len(letters) > 0:
	nodes.append(letters[0:2])
	letters = letters[2:]
nodes.sort()
huffman_tree = []
huffman_tree.append(nodes)

def combine(nodes):
	pos = 0
	newnode = []
	if len(nodes) > 1:
		nodes.sort()
		nodes[pos].append("0")
		nodes[pos+1].append("1")
		combined_node1 = (nodes[pos][0]+nodes[pos+1][0])
		combined_node2 = (nodes[pos][1]+nodes[pos+1][1])
		newnode.append(combined_node1)
		newnode.append(combined_node2)
		newnodes = []
		newnodes.append(newnode)
		newnodes = newnodes + nodes[2:]
		nodes = newnodes
		huffman_tree.append(nodes)
		combine(nodes)
	return huffman_tree


newnodes = combine(nodes)

huffman_tree.sort(reverse=True)
#print "dentro:"

checklist = []
for level in huffman_tree:
	for node in level:
		if node not in checklist:
			checklist.append(node)
		else:
			level.remove(node)
#count = 0
#for level in huffman_tree:
#	print "Level", count, ":", level
#	count += 1
#print " "

letter_binary = []
if len(only_letters) == 1:
	letter_code = [only_letters[0], "0"]
	letter_binary.append(letter_code * len(my_string))
else:
	for letter in only_letters:
		lettercode = ""
		for node in checklist:
			if len(node) > 2 and letter in node[1]:
				lettercode = lettercode + node[2]
		letter_code = [letter,lettercode]
		letter_binary.append(letter_code)

#print "Your binary codes are as follows:"
#for letter in letter_binary:
#	print letter[0], letter[1]

bitstring = ""
for character in my_string:
	for letter1 in letter_binary:
		if character in letter1:
			bitstring = bitstring + letter1[1]

binary = bin(int(bitstring, base = 2))

uncompressed_file_size = len(my_string) * 8
compressed_file_size = len(binary) - 2

#print "compressed binary", bitstring
#print "to original file htan", uncompressed_file_size ,"bits"
#print "to compressed file htan" , compressed_file_size,"bits"
#print "swzei:" , uncompressed_file_size-compressed_file_size,"bits"
#print binary

#data_b2a = binascii.b2a_uu(binary)
#print "**Binary to Ascii** \n"
#print data_b2a



G = matrix(GF(2), matrix1)
C = LinearCode(G)

vecs = []
count = 0
while count < len(bitstring):
	if count % C.dimension() == 0:
		vecs.append([])
	vecs[-1].append(int(bitstring[count]))
	count += 1
while count % C.dimension() != 0:
	vecs[-1].append(0)
	count += 1

err = min(err, int((C.minimum_distance() - 1)/2))
chan = channels.StaticErrorRateChannel(C.ambient_space(), err)
rw = ""
for vec in vecs:
	vect = vector(GF(2), vec)
	c = C.encode(vect)
	r = chan.transmit(c)
	for point in r:
		rw += str(point)

#print "received word", rw

encodedReceivedWord = en_to_base64(rw)

#print "encoded word", encodedReceivedWord

data = {}
data["encodedword"] = encodedReceivedWord
compressiondict = {}
compressiondict["name"] = "Huffman"
compressiondict["keys"] = letter_binary
data["compression_algorithm"] = compressiondict
codedict = {}
codedict["name"] = "linear"
codedict["P"] = my_matrix
data["code"] = codedict

writeToJSONFile("./", "data", data)


#Receiver


with open("data.json") as json_file:  
    data = json.load(json_file)

received_word = data["encodedword"]

#print "\nencoded64 word", received_word

received_word = base64.b64decode(received_word) #decode base64

#print "decoded64 word", received_word

# decode linear

G_receiv = matrix(GF(2), ast.literal_eval(data["code"]["P"]))
C_receiv = LinearCode(G_receiv)

vecs = []
count = 0
while count < len(received_word):
	if count % C_receiv.length() == 0:
		vecs.append([])
	vecs[-1].append(int(received_word[count]))
	count += 1
while count % C_receiv.length() != 0:
	vecs[-1].append(0)
	count += 1

unencWord = ""
for vec in vecs:
	vect = vector(GF(2), vec)
	unencodedWord = C.decode_to_message(vect)
	for point in unencodedWord:
		unencWord += str(point)

#print vecs

#print "unencoded word", unencWord

#decompress huffman


#MARIE EDW EXEI LA8OS

letter_translator = data["compression_algorithm"]["keys"]
index = 0
j = 0
originalWord = ""
try:
    while index < len(unencWord):
        j += 1
    	checker_sequence = ""
    	for i in xrange(index, index + j):
    	    checker_sequence += unencWord[i]
    	for letter1 in letter_translator:
    		if checker_sequence == letter1[1]:
    			originalWord += letter1[0]
    			index += j
    			j = 0
except:
    x = "lol"

print "original word", originalWord