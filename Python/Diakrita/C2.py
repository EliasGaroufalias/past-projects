from math import *

try:
	met = raw_input("\nDwse metathesh:\n\n\t")
	met = [int(i) for i in met.split(' ')]
except:
	print '\nWrong Input.'	
	exit()
for i in met:
	if i > len(met) or i < 1 :
		print '\nWrong Input.'
		exit()

rank = 0
for i in xrange(len(met)):
	for j in xrange(i, len(met)):
		if met[i] < met[j]:
			met[j] -= 1
	rank += (met[i]-1)*factorial(len(met)-i-1)
print '\nTo rank ths metatheshs einai', rank