from time import *
from math import *
from graphics import *

try:
	n = int(raw_input('\nGive me the number n for which I shall draw Kn. Large numbers encouraged.\n\n\t'))
except:
	print 'Wrong Input.'
	exit()
if n == 2:
	print '\n[0, 1]'
elif n % 2 == 0:
	print '\nN is an even number. No Euler road can be found.'
elif n == 1:
	print '\n[0]'
elif n < 1:
	print '\nNegative value of n'
else:
	if n == 3:
		road = [[0,1], [1,2], [2,0]]
	else:
		road = []
		last_point = 0
		for j in xrange(n):
			for i in xrange(1, int(n/2) + 1):
				road.append([last_point, (last_point + i) % n])
				last_point = (last_point + i) % n
	if n < 100:
		print '\nOne Euler road for K'+str(n)+' is as follows:\n\n(', road[0][0] + 1,
		for i in road:
			print i[1] + 1,
		print ')\n'
	else:
		print "\n\tn is too big for the euler road to fit in cmd."

	size = n*7 + 250
	size = size*(size<1280)+1280*(size>=1280)

	win = GraphWin('Euler Road', size, size)
	win.setBackground(color_rgb(0,0,0))

	pts = []
	for i in xrange(n):
		pts.append(Point(cos(radians((float(i)/float(n))*360))*(size/2) + size/2, sin(radians((float(i)/float(n))*360))*(size/2) + size/2))
	pol = Polygon(pts)
	pol.setFill(color_rgb(0,150,225))
	pol.draw(win)

	sleeptime = float(1-(float(n)/float(100)))/float(15)*(n < 100)
	for i in xrange(len(road)):
		ln = Line(pts[road[i][0]],pts[road[i][1]])
		ln.setOutline(color_rgb(255,0,0))
		ln.draw(win)
		sleep(sleeptime)
	win.getMouse()
	win.close()