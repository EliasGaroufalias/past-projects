def Half_chance(n, prnt = False):
	value = 1
	for i in xrange(n):
		value *= float(n-i)/float(n)
		if prnt:
			print float(1-value), i+1
		if value >= 0.5:
			number_required = i+2
	return number_required

n = Half_chance(int(raw_input("\n\tGive the number of possible choices:\n\n\t\t")), True)
print "\n With", n, "choices made, there is a >=50% chance that two are identical."