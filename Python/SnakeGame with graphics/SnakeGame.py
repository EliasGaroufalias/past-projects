from graphics import *
import random

class Snake_Pixel(Rectangle):

	global food_pos
	global snek_pos
	time = 0
	tile_size = 0

	def __init__(self, p1, p2, time, tile_size):
		Rectangle.__init__(self, p1, p2, time)
		self.time = time
		self.tile_size = tile_size

	def moved(x, y, time):
		if (x*tile_size == self.x) and (y*tile_size == self.y):
			self.setFill(color_rgb(255,255,255))
			self.time = time
		elif (x == food_pos[0]) and (y == food_pos[1]):
			self.time += 0
		else:
			self.time -= 1
		if time == 0:
			self.setFill(color_rgb(0,0,0))


class Snake():

	global snek_pos
	size = 1

	def __init__(self, resolution):
		self.pos = [random.randint(1,resolution-1),random.randint(1,resolution-1)]
		self.size = 1

	def move(self, direction):
		#print ' ',(direction+1)%2,direction%2, ((-1)**((direction+1)%2)),
		self.pos[0] += (direction+1)%2*((-1)**((direction//2+1)%2))
		self.pos[1] += direction%2*((-1)**((direction//2)%2))
		snek_pos = list(self.pos)
		pass
		

snek_pos = []
size = 800
resolution = 32
tile_size = size//resolution
size = tile_size*resolution

snek = Snake(resolution)
print snek.pos
print snek_pos
snek.move(0)
print snek.pos
print snek_pos
snek.move(1)
print snek.pos
snek.move(2)
print snek.pos
snek.move(3)
print snek.pos

win = GraphWin('Snake Game', size, size)
win.setBackground(color_rgb(0,0,0))

tiling = []
tiles = []
for i in xrange(resolution):
	for j in xrange(resolution):
		tiles.append(Rectangle(Point(tile_size*i,tile_size*j),Point(tile_size*(i+1),tile_size*(j+1))))
	tiling.append(list(tiles))
	tiles = []
for j in tiling:
	for i in j:
		i.setFill(color_rgb(0,0,0)) 
		i.setOutline(color_rgb(150,150,150))
		i.draw(win)
tiling[snek.pos[0]][snek.pos[1]].setFill(color_rgb(255,255,255))

win.getMouse()
win.close()