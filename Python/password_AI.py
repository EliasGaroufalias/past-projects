from __future__ import print_function
import random
import string

def bubbleSortFitness(alist):
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if alist[i].fit>alist[i+1].fit:
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp

def fitness (password, test_word):
	score = 0
	i = 0
	while (i < len(password) and i < len(test_word)):
		if (password[i] == test_word[i]):
			score+=1
		i+=1
	if score == len(password):
		while i < len(test_word):
			score *= 0.9
			i += 1
	return 50*(len(password) == len(test_word))  + (score * 50 // len(password))

class Creature:
	length = 0
	letters = []
	fit = 0

	def __init__(self, length, letters):
		self.length = length
		self.letters = letters
		self.fit = fitness('Hello_World',letters)


n = int(raw_input("\n\tGive me the number of specimen in each generation:\n\n\t\t"))
maxsize = int(raw_input("\n\tGive me the maximum desirable size of each specimen:\n\n\t\t"))
alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','_','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
specimens = []
for i in xrange(n):
	letters = ''
	length = random.randint(1,maxsize)
	for _ in xrange(length):
		letters += random.choice(alphabet)
	specimens.append(Creature(length, letters))

bubbleSortFitness(specimens)
print('')
while specimens[-1].fit != 100:
	for i in xrange(n//2):
		specimens.pop(0)
	i = 0
	while len(specimens)<n:
		letters = ''
		lengthchoice = [random.randint(1,maxsize), len(specimens[i].letters), len(specimens[i+1].letters), len(specimens[i].letters), len(specimens[i+1].letters), len(specimens[i].letters), len(specimens[i+1].letters)]
		length = random.choice(lengthchoice)
		for j in xrange(length):
			if j < len(specimens[i].letters) and j < len(specimens[i+1].letters):
				letterchoice = [specimens[i].letters[j], specimens[i+1].letters[j], specimens[i].letters[j], specimens[i+1].letters[j], specimens[i].letters[j], specimens[i+1].letters[j], random.choice(alphabet)]
			else:
				letterchoice = random.choice(alphabet)
			letters += random.choice(letterchoice)
		specimens.append(Creature(length, letters))
		i += 1
	bubbleSortFitness(specimens)
	i = specimens[-1]
	print('', i.fit, end='%\t')
	for j in i.letters:
		print(j, end='')
	print('')