import tweepy
import json
from tweepy import OAuthHandler
from nltk.tokenize import word_tokenize
from collections import Counter
import re
from nltk.corpus import stopwords
import string


def write_tweets(tweets, filename):
    with open(filename, 'a') as f:      #function to write statuses into a username.json file
        json.dump(tweets._json, f)
        f.write('\n')

consumer_key = ''
consumer_secret = ''
access_token = ''
access_secret = ''
                                                                     #authorize app to access tweeter on my behalf
auth = OAuthHandler(consumer_key, consumer_secret)                   #using the oAuth interface
auth.set_access_token(access_token, access_secret)

api = tweepy.API(auth)                  #api variable is now our entry point

user_name = raw_input('dwse onoma xrhsth e.g.realDonaldTrump')
for status in tweepy.Cursor(api.user_timeline, screen_name=user_name, tweet_mode = 'extended').items(10):   #
    write_tweets(status, user_name+'.json')


                                                    #tokenization and regexes
emoticons_str = r"""
    (?:
        [:=;]                          
        [oO\-]? #
        [D\)\]\(\]/\\OpP]   
    )"""

regex_str = [
    emoticons_str,
    r'<[^>]+>',  #HTML tags
    r'(?:@[\w_]+)',  #@-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)",  #hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+',  #URLs

    r'(?:(?:\d+,?)+(?:\.?\d+)?)',  #numbers
    r"(?:[a-z][a-z'\-_]+[a-z])",  #words with - and '
    r'(?:[\w_]+)',  # ther words
    r'(?:\S)'  #anything else
]

tokens_re = re.compile(r'(' + '|'.join(regex_str) + ')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^' + emoticons_str + '$', re.VERBOSE | re.IGNORECASE)


def tokenize(s):
    return tokens_re.findall(s)


def preprocess(s, lowercase=False):
    tokens = tokenize(s)
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens
#with preprocess everything is tokenized, links, mentions, hastags, even emoticons

punctuation = list(string.punctuation)
stop = stopwords.words('english') + punctuation + ['rt', 'via']     #remove white noise from common stop words like and so the
                                                                    #punctuation and the rt and via words for retweet and quotations


filename = user_name + '.json'
with open(filename, 'r') as f:
    count_all = Counter()
    for line in f:
        tweet = json.loads(line)
        terms_all = [term for term in preprocess(tweet['full_text']) if term not in stop]
        count_all.update(terms_all) #updating counter
    final_tuple = (count_all.most_common(1))
    print final_tuple




