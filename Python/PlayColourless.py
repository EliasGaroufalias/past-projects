from ctypes import windll
import pyautogui
import time
import random


def BubbleSortFitness(alist):
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if alist[i].fit>alist[i+1].fit:
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp


class Play:
	
	commands = []
	fit = 0

	def __init__(self, commands):
		self.commands = commands

	def play_it:
		start_time = time.time()
		for i in xrange(0, length(self.commands), 2):
			pyautogui.keyDown(self.commands[i])
			time.sleep(self.commands[i+1])
			pyautogui.keyUp(self.commands[i])
		i += 2
		while not dead():
			self.commands.append(random.choice(pyautogui.KEYBOARD_KEYS))
			i += 1
			pyautogui.keyDown(self.commands[i])
			self.commands.append(random.uniform(0.0,1.0))
			i += 1
			time.sleep(self.commands[i+1])
			pyautogui.keyUp(self.commands[i-1])
		self.fit = time.time() - start_time

	def dead():




def main():

	past_gens = [Play([]), Play([])]
	while not won():
		commands = []
		for i in past_gens[-1].commands:
			controls_choice = [past_gens[-1].commands[i], past_gens[-2].commands[i], random.choice(pyautogui.KEYBOARD_KEYS)
			commands.append(random.choice(controls_choice))
		past_gens.append(Play(commands))
		past_gens[-1].play_it()
		BubbleSortFitness(past_gens)