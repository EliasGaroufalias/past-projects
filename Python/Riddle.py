import random


def printPlans():
	for i in xrange(len(plans)):
		for j in plans[i].plan_tuple:
			if j == '\n':
				print '\n',
				break
			else:
				print j,


def bubbleSortFitness(alist):
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if alist[i].fitness<alist[i+1].fitness:
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp


class plan():
	twelve = 1
	finn = 2
	caleb = 4
	gaten = 7

	real = []
	reverse = [twelve, finn, caleb, gaten]

	fitness = 0
	victor = ' '

	plan_id = 0
	c11 = 0
	c12 = 0
	c2 = 0
	c31 = 0
	c32 = 0
	c4 = 0
	c51 = 0
	c52 = 0

	def __init__(self, c11, c12, c2, c31, c32, c4, c51, c52, plan_id):
		self.real = []
		self.reverse = [self.twelve, self.finn, self.caleb, self.gaten]
		self.cross_over(c11 , c12)
		self.cross_back(c2 )
		self.cross_over(c31 , c32)
		self.cross_back(c4 )
		self.cross_over(c51 , c52)
		self.plan_id = plan_id
		time=self.fitness
		if self.c31==1 or self.c32==1:
			self.fitness+=2
		if self.fitness<15:
			self.victor = '<---  Made it!'
		self.plan_tuple = (
			'id= ',	plan_id, ' ', '\t',
			self.c11,self.c12,'   ',self.c2,'   ',self.c31,self.c32,'   ',self.c4,'   ',self.c51,self.c52,
			'\t','time=',time,
			self.victor,'\n',
			c11,c12,c2,c31,c32,c4,c51,c52,' ')
#		print '\n\n'

	def cross_over(self, p1, p2):
		if p1>p2:
			temp = p1
			p1 = p2
			p2 = temp
#		print self.reverse, self.real
#		print self.reverse[p1], self.reverse[p2], '\t\t\t', self.fitness
		if self.c11 == 0:
			self.c11 = self.reverse[p1]
			self.c12 = self.reverse[p2]
		elif self.c31 == 0:
			self.c31 = self.reverse[p1]
			self.c32 = self.reverse[p2]
		elif self.c51 == 0:
			self.c51 = self.reverse[p1]
			self.c52 = self.reverse[p2]
		self.fitness += max(self.reverse[p1], self.reverse[p2])
		self.real.append(self.reverse.pop(p1))
		self.real.append(self.reverse.pop(p2-1))

	def cross_back(self, p2):
#		print self.reverse, self.real
#		print '\t', self.real[p2],'\t\t', self.fitness
		if self.c2 == 0:
			self.c2 = -self.real[p2]
		elif self.c4 == 0:
			self.c4 = -self.real[p2]
		self.fitness += self.real[p2]
		self.reverse.append(self.real.pop(p2))

Laurel = input("\n\n\tThere is a canyon with a bridge that can support a maximum of 2 people. A group of 4 needs to cross over it\nThe first person named Laurel needs *input* minutes to cross it.\n\t")
yanny = 


plans = []
#allplans = []
n=15
for i in xrange(n):
	param = []
	for j in xrange(3):
		param.append(random.randint(1,3-j))
		param.append(random.randint(0,3-j))
		while param[-1]==param[-2]:
			param[-1] = random.randint(0,3-j)
		param.append(random.randint(0,1+j))
#	allplans.append(list(param))
	plans.append(plan(param[0], param[1], param[2], param[3], param[4], param[5], param[6], param[7], i))

Gen=1
bubbleSortFitness(plans)
while plans[-1].victor == ' ':
	print '\nGen',Gen,'\n'
	printPlans()

	for i in xrange(4*len(plans)//5):
		plans.pop(0)

	i = 0
	while len(plans)<n:
		param = []
		for j in xrange(3):
			planchoice1 = [
					plans[i].plan_tuple[21+3*j],
					plans[i+1].plan_tuple[21+3*j],
					random.randint(1,3-j)]
			planchoice2 = [
					plans[i].plan_tuple[22+3*j],
					plans[i+1].plan_tuple[22+3*j],
					random.randint(0,3-j)]
			planchoice3 = [
					plans[i].plan_tuple[23+3*j],
					plans[i+1].plan_tuple[23+3*j],
					random.randint(0,1+j)]
			param.append(random.choice(planchoice1))
			param.append(random.choice(planchoice2))
			while param[-1]==param[-2]:
				param[-1] = random.randint(0,3-j)
			param.append(random.choice(planchoice3))
		plans.append(plan(param[0], param[1], param[2], param[3], param[4], param[5], param[6], param[7], n+i*Gen))
		i+=1

	bubbleSortFitness(plans)
	Gen +=1

print '\nGen',Gen,'\n'
printPlans()