precede_list( [], [_] ).
precede_list( [H|T], [H|TT] ) :- precede_list(T, TT).