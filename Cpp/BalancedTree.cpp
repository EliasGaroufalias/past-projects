template <class T>
class Stack {
	public:
		Stack(int MaxStackSize) {
			MaxTop = MaxStackSize - 1;
			stack = new T[MaxStackSize];
			top = -1;
		}
		~Stack() {delete [] stack;}
		bool IsEmpty() const {return top == -1;}
		bool IsFull() const {return top == MaxTop;}
		T Top() const {
			if (IsEmpty()) throw OutOfBounds();
			else return stack[top];
		} 
		Stack<T>& Add(const T& x) {
			if (IsFull()) throw NoMem();
			stack[++top] = x;
			return *this;
			}
		Stack<T>& Pop(T& x) {
			if (IsEmpty()) throw OutOfBounds();
			x = stack[top--];
			return *this;
		}
	private:
		int top;
		int MaxTop;
		T *stack;
}




template <class T>
class BinaryTreeNode {
	friend class BinaryTree
	public:
		BinaryTreeNode() {LeftChild = RightChild = 0;}
		BinaryTreeNode(const T& e)
		{data = e; LeftChild = RightChild = 0;}
		BinaryTreeNode(const T& e, BinaryTreeNode *l,
		BinaryTreeNode *r)
		{data = e; LeftChild = l; RightChild = r;}
	private:
		T data;
		BinaryTreeNode<T> *LeftChild, *RightChild;
	};




template <class T>
class BinaryTree {
	public:
		BinaryTree(double b, double c) {
			root = 0;
			b = b;
			c = c;
			d = 0;
			n = 0
		};
		~BinaryTree() { };
		bool IsEmpty () const {
		return ((root) ? false : true);
		}
		bool Root (T& x) const {
			if (root) {x = root->data; return true;}
			else return false;
		}

		template <class K, class E>
		bool Search(const K& k, E& e, int& dep, int& h) const {
			dep = 1;
			h = 1;
			bool found = false;
			BinaryTreeNode<E> *p = root;
			while (p) {
				if (found) dep+=1;
				else {
					h += 1;
					if (k < p->data) p = p->LeftChild;
					else if (k > p->data) p = p->RightChild;
					else {
						e = p;
						found = true;
					}
				}
			}
			dep *= found;
			h *= found;
			return found;
		}

		template <class K4, class E4>
		void CheckReconstruct(const K4& k, int depth) const {
			if (depth > c*(log(n+1+d)/log(2)) ) Reconstruct(k, depth);
			return 0;
		}

		template <class K1, class E1>
		BinaryTree<E1,K1>& InsertTo(BinaryTreeNode<E1> *p, const E1& e, bool flag) {
			*pp = 0;
			int depth = 1;
			while (p) {
				depth++;
				pp = p;
				if (e < p->data) p = p->LeftChild;
				else if (e > p->data) p = p->RightChild;
				else {
					depth--;
					throw BadInput(); 
				}
			}
			BinaryTreeNode<E1> *r = new BinaryTreeNode<E1> (e);
			if (root) {
				if (e < pp->data) pp->LeftChild = r;
				else pp->RightChild = r;
			}
			else root = r;
			k = r;
			n++;
			if (flag) CheckReconstruct(e, depth);
			return *this;
		}
		
		template <class K2, class E2>
		BinaryTree<E2,K2>& Delete(const K2& k, E2& e) {
			BinaryTreeNode<E2> *p = root, *pp = 0;
			while (p && p->data != k){
				pp = p;
				if (k < p->data) p = p->LeftChild;
				else p = p->RightChild;
			}
			if (!p) throw BadInput();
			e = p->data;
			if (p->LeftChild && p->RightChild) {
				BinaryTreeNode<E2> *s = p->LeftChild,
				*ps = p;
				while (s->RightChild) {
					ps = s;
					s = s->RightChild;
				}
				p->data = s->data;
				p = s;
				pp = ps;
			}
			BinaryTreeNode<E2> *c;
			if (p->LeftChild) c = p->LeftChild;
			else c = p->RightChild;
			if (p == root) root = c;
			else {
				if (p == pp->LeftChild)
				pp->LeftChild = c;
				else pp->RightChild = c;
			}
			delete p;
			return *this;
		}

		template <class K5, class E5>
		BinaryTree<E5,K5>& InsertTree(BinaryTreeNode<E5> *p, T* array1, int ind) {
			index = 0
			int i = ind
			for (int j = 0; pow(10,j) <= i; j++) {
				if (i%pow(10,i)) index += ceil((array1.size())/pow(2,i));
				i = floor(i/10);
			}
			InsertTo(p, array1[index]);
			InsertTree(p, *array1, i*10);
			InsertTree(p, *array1, i*10+1);
		}

		template <class K3, class E3>
		BinaryTree<E3,K3>& Reconstruct(const K3& k, int depth) {
			stack = new Stack(depth);
			BinaryTreeNode<E> *p = root;
			while (p && p->data != k) {
				stack.Add(p)
				if (k < p->data) p = p->LeftChild;
				else if (k > p->data) p = p->RightChild;
			}
			deconstructed = new T[depth];
			//ftiakse array pou na einai ta3inomhmeno kai na exei ola ta stoixeia tou ypodentrou
			//prepei na kanoyme diatre3h me endodiata3h, ayth se dyadiko dentro soy ta diabazei me thn seira, ta3inomhmena
			BinaryTreeNode<E3> *subroot = 0;
			int d1=0;
			int h1=0;
			InsertTo(root, deconstructed[ceil(depth/2)]);
			Search(deconstructed[ceil(depth/2)], subroot, d1, h1);
			InsertTree(subroot, *deconstructed, 1);
			return *this;
		}
	private:
		BinaryTreeNode<T> *root;
};