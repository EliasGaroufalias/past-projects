#include <iostream>
using namespace std;

class BankCard {
    public:
        BankCard(string bN = "", string hN = "", int y = 2000) {
            bankName = bN;
            holderName = hN;
            year = y;
        }
        string GetBank() {
            return bankName;
        }
        string GetHolder() {
            return holderName;
        }
        int GetYear() {
            return year;
        }
        void SendAlert(int checkYear) {
            if (checkYear == year) {
                cout <<  "Teleytaios xronos gia thn karta toy " << holderName << " sthn " << bankName << endl;
            }
        }
    private:
        string bankName;
        string holderName;
        int year;
};

class CreditCard : public BankCard {
    public:
        CreditCard(int cL, int t, string bN = "", string hN = "", int y = 2000) : BankCard(bN, hN, y) {
            creditLimit = cL;
            type = t;
        }
        int GetLimit() {
            return creditLimit;
        }
        int NoDoses() {
            bool isGold = (type == 2);
            return (isGold ? 60 : 30);
        }
        void Print() {
            bool isGold = (type == 2);
            cout << "Bank name: " << this->GetBank() << "\nHolder name: " << this->GetHolder() << "\nExpiration date: " << this->GetYear() << "\nCredit limit: " << creditLimit << "\nCard type: " << (isGold ? 60 : 30) << endl;
            
        }
    private:
        int creditLimit;
        int type;
};

int main() 
{
    CreditCard cc1 = CreditCard(3000, 1, "Bank1", "John Smith", 2017);
    CreditCard cc2 = CreditCard(1200, 2, "Bank1", "John Smith", 2019);
    CreditCard cc3 = CreditCard(4000, 1, "Bank2", "John Smith", 2020);
    CreditCard ccs[3] = {
        cc1, cc2, cc3
    };
    int sum = 0;
    for (int i = 0; i < 3; i++) {
        ccs[i].SendAlert(2019);
        sum += ccs[i].GetLimit();
    }
    cout << "Total credit limit: " << sum << endl;
}