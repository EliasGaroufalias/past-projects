# ==================== LEXICAL ANALYSIS ====================

def Lexically_Analyze(textfile):

	f = open(textfile)


	# Διαβασμα κειμένου, αφαιρώντας τους χαρακτήρες με τις ακόλουθες τιμές ASCII
	non_important_chars = "".join(
									[chr(i) for i in range(0, 32)]
					    			+ [chr(i) for i in range(33, 46)]
					    			+ [chr(i) for i in range(58, 65)]
					    			+ [chr(i) for i in range(91, 97)]
					    			+ [chr(i) for i in range(123, 128)]
					    		)
	story = ''.join(char for char in f.read() if char not in non_important_chars)


	# αφαίρεση κενών στην αρχή πρότασης
	rough_sentences = story.split('.')
	sentences = []
	for i in range(len(rough_sentences)):
		sentence = rough_sentences[i]
		if len(sentence) > 0:
			if sentence[0] == chr(32):
				sentence = sentence.replace(chr(32), '', 1)
			sentences.append(sentence)


	# χωρισμός προτάσεων σε λέξεις
	words = []
	for sentence in sentences:
		sentence_words = []
		sentence_words.append(sentence.split(' '))
		words.append(sentence_words[-1])


	f.close()


	#εκτύπωση
	print("\n\n{=====} SENTENCES RESULTING FROM LEXICAL ANALYSIS {=====}\n\n")
	for sentence in words:
		print(sentence)

	return words

# ==================== END OF LEXICAL ANALYSIS ====================



# ==================== SYNTACTICAL ANALYSIS ====================


# 	=== VOCABULARY ===

verb = ['loves', 'love']
intr_v = ['runs', 'run', 'running']
aux_v = ['is', 'are']
trans_v = ['gives', 'give', 'gave']

adverb = ['quickly']

adjective = ['tall', 'slim', 'blonde']

noun = ['book', 'books', 'dog', 'dogs']
proper_n = ['mary', 'john', 'tomy']

det = ['a', 'an', 'the']


#	=== Fuction commonly used for grammar ===

def analyse_np(sentence, structure, str_sentence):
	structure = ['np', []]
	str_sentence = []

	if sentence[0] in det:
		structure[-1].append('det')
		str_sentence.append(sentence.pop(0))
		structure[-1].append('noun')
		str_sentence.append(sentence.pop(0))
		# κάνουμε pop στο στοιχείο που χρησιμοποιήσαμε, ετσι ωστε να δουλεύουμε πάντα με το sentence[0]

	elif sentence[0] in noun:
		structure[-1].append('noun')
		str_sentence.append(sentence.pop(0))

	elif sentence[0] in proper_n:
		structure[-1].append('proper_n')
		str_sentence.append(sentence.pop(0))

	return sentence, structure, str_sentence


def Syntactically_Analyze(story):

#	=== TREE BUILDING ===

	# structure: η δομή της πρότασης. Θα έχει μορφή [['np', ['proper_n']],['vp', ['intr_v', 'adverb']]]
	# str_sentence (= structured sentence): η πρόταση σε δομημένη μορφή. Θα εχει μορφή [['mary'], ['runs', 'quickly']]
	# (σημείωση: list[-1] είναι το τελευταίο entry της λίστας list)


	print("\n\n\n\n{=====} GRAMMAR TREES RESULTING FROM SYNTACTICAL ANALYSIS {=====}\n\n")
	structures = []
	sentences = []
	for sentence in story:
		structure = []
		str_sentence = []

		# Ανάλυση noun phrase

		sentence, added_structure, added_str_sentence = analyse_np(sentence, structure, str_sentence)		
		structure.append(added_structure)
		str_sentence.append(added_str_sentence)

		# Ανάλυση verb phrase

		structure.append(['vp', []])
		str_sentence.append([])

		if sentence[0] in verb:
			structure[-1][-1].append('verb')
			str_sentence[-1].append(sentence.pop(0))
			sentence, added_structure, added_str_sentence = analyse_np(sentence, structure, str_sentence)		
			structure[-1][-1].append(added_structure)
			str_sentence[-1].append(added_str_sentence)

		elif sentence[0] in intr_v:
			structure[-1][-1].append('intr_v')
			str_sentence[-1].append(sentence.pop(0))
			if len(sentence) != 0:
				structure[-1][-1].append('adverb')
				str_sentence[-1].append(sentence.pop(0))

		elif sentence[0] in aux_v:
			structure[-1][-1].append('aux_v')
			str_sentence[-1].append(sentence.pop(0))
			structure[-1][-1].append('adjective')
			str_sentence[-1].append(sentence.pop(0))

		elif sentence[0] in trans_v:
			structure[-1][-1].append('trans_v')
			str_sentence[-1].append(sentence.pop(0))
			sentence, added_structure, added_str_sentence = analyse_np(sentence, structure, str_sentence)		
			structure[-1][-1].append(added_structure)
			str_sentence[-1].append(added_str_sentence)
			sentence, added_structure, added_str_sentence = analyse_np(sentence, structure, str_sentence)		
			structure[-1][-1].append(added_structure)
			str_sentence[-1].append(added_str_sentence)

		structures.append(structure)
		sentences.append(str_sentence)


	# εκτύπωση
	for i in range(len(structures)):
		for j in range(len(structures[i])):
			if j == 1 and type(structures[i][j][1][1]) is list: 
				print('[\'vp\', [')
				for k in range(len(structures[i][j][1])):
					print('\t', structures[i][j][1][k], '\t', sentences[i][j][k])
				print(']')
			else: 
				print(structures[i][j], '\t', sentences[i][j])
		print('\n')

	return structures, sentences 


# ==================== END OF SYNTACTICAL ANALYSIS ====================



# ==================== Main ====================

Syntactically_Analyze(Lexically_Analyze('mary.txt'))