/* Definite clause Grammars Code */  
s --> np, vp.
np --> det, noun.
vp --> verb, np.
det --> [the].
verb --> [brought].
noun --> [waiter].
noun --> [meal].
