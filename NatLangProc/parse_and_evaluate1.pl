
expression(Value) --> number(Value).
expression(Value) --> 
	number(X), [+], expression(V),	{Value is X+V}.
expression(Value) --> 
	number(X), [-], expression(V),	{Value is X-V}.
expression(Value) --> 
	number(X), [*], expression(V),	{Value is X*V}.
expression(Value) --> 
	number(X), [/], expression(V),	{V\=0, Value is X/V}.
expression(Value) --> left_parenthesis, expression(Value), right_parenthesis.
left_parenthesis --> ['('].
right_parenthesis --> [')'].

number(X) --> digit(X).
number(Value) --> digit(X), number(Y), 
	{numberofdigits(Y,N), Value is X*10^N+Y}.
digit(0) --> [0].
digit(1) --> [1].
digit(2) --> [2]. 
digit(3) --> [3]. 
digit(4) --> [4]. 
digit(5) --> [5]. 
digit(6) --> [6]. 
digit(7) --> [7]. 
digit(8) --> [8]. 
digit(9) --> [9].

numberofdigits(Y,1) :- Z is Y/10, Z<1, !.
numberofdigits(Y,N) :- 
	Z is (Y - mod(Y,10))/10,
	numberofdigits(Z,N1), 
	N is N1+1, !.

recognize(Input, Value) :-expression(Value,Input,[]).






