s(s(NP,VP))  --> np(NP), vp(VP).
np(np(D,N))  --> det(D), noun(N).
vp(vp(V,NP)) --> verb(V), np(NP).
det(det(the)) --> [the].
verb(verb(brought)) --> [brought].
noun(noun(waiter)) --> [waiter].
noun(noun(meal)) --> [meal].

