﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory_Pattern
{
    class EnemyFactory
    {
        public static Enemy GetEnemy(string enemyType)
        {
            if (enemyType == "Goblin") return new Goblin();
            else if (enemyType == "Bandit") return new Bandit();
            else return null;
        }
    }
}
