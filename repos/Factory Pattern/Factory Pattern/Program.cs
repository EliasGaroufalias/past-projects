﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            List<string> possibleEnemies = new List<string> { "Goblin", "Bandit" };
            for (int i = 0; i < 5; i++)
            {
                Enemy enemy = EnemyFactory.GetEnemy(possibleEnemies[rnd.Next(2)]);
                enemy.Attack();
            }
            Console.ReadKey();
        }
    }
}
