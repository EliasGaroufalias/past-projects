﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory_Pattern
{
    class Bandit : Enemy
    {
        int hp;
        int damage;

        public override void Attack() { Console.WriteLine("The Bandit swipes with his sword!"); }

        public override int getDamage() { return damage;}
        public override int getHp() { return hp; }

        public override void setDamage(int hp) { this.hp = hp; }
        public override void setHp(int dmg) { damage = dmg; }
    }
}
