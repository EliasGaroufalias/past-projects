﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory_Pattern
{
    abstract class Enemy
    {
        int hp;
        int damage;

        public abstract int getHp();
        public abstract int getDamage();
        public abstract void setHp(int hp);
        public abstract void setDamage(int dmg);

        public abstract void Attack();
    }
}
