﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsDIP
{
    class Program
    {
        static void Main(string[] args)
        {
            AttackPlan attackPlan = new AttackPlan();
            int playerId = 1;
            List<int> goblinIds = new List<int> { 1 };
            List<int> banditIds = new List<int> { 1 };

            string plan = attackPlan.MakePlan(goblinIds, banditIds, playerId);
            Console.WriteLine("Player" + playerId.ToString() + " chooses to " + plan + ".");
            Console.ReadKey();
        }
    }
}
