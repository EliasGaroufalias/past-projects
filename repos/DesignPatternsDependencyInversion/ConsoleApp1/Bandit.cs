﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsDIP
{
    class Bandit : IDataAccess
    {
        public Bandit() { }

        public int GetHp(int id) { return 12; }
        public int GetDamage(int id) { return 4; }
    }
}
