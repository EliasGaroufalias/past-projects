﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsDIP
{
    class Goblin : IDataAccess
    {
        public Goblin() { }

        public int GetHp(int id) { return 9; }
        public int GetDamage(int id) { return 5; }
    }
}
