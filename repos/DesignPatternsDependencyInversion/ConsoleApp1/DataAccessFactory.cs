﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsDIP
{
    class DataAccessFactory
    {
        public static IDataAccess GetEnemyDataAccessObj(string enemyType)
        {
            if (enemyType == "Goblin") return new Goblin();
            else if (enemyType == "Bandit") return new Bandit();
            else if (enemyType == "Player") return new Player();
            else return null;
        }
    }
}
