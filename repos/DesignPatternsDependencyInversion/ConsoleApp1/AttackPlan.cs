﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsDIP
{
    class AttackPlan
    {
        IDataAccess goblinDataAccess;
        IDataAccess banditDataAccess;
        IDataAccess playerDataAccess;

        public AttackPlan()
        {
            goblinDataAccess = DataAccessFactory.GetEnemyDataAccessObj("Goblin");
            banditDataAccess = DataAccessFactory.GetEnemyDataAccessObj("Bandit");
            playerDataAccess = DataAccessFactory.GetEnemyDataAccessObj("Player");
        }

        public string MakePlan(List<int> goblinIds, List<int> banditIds, int playerId)
        {
            string plan = "";

            int expectedDamage = 0;
            foreach (int Id in goblinIds)
            {
                expectedDamage += goblinDataAccess.GetDamage(Id);
            }
            foreach (int Id in banditIds)
            {
                expectedDamage += banditDataAccess.GetDamage(Id);
            }

            if (expectedDamage > playerDataAccess.GetHp(playerId)) plan = "retreat";
            else plan = "attack";

            return plan;
        }

    }
}
