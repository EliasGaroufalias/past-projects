﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsDIP
{
    class Player : IDataAccess
    {
        public Player() { }

        public int GetHp(int id) { return 25; }
        public int GetDamage(int id) { return 7; }
    }
}
