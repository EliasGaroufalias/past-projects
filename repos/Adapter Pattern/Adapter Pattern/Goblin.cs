﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Pattern
{
    class Goblin : Enemy
    {
        int hp;
        int damage;

        public override void Attack() { Console.WriteLine("The Goblin attacks with its nails!"); }

        public override int getDamage() { return damage; }
        public override int getHp() { return hp; }

        public override void setHp(int hp) { this.hp = hp; }
        public override void setDamage(int dmg) { damage = dmg; }
    }
}