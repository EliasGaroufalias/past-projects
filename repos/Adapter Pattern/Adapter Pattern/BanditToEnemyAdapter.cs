﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Pattern
{
    class BanditToEnemyAdapter : Enemy
    {
        Bandit bandit;

        public BanditToEnemyAdapter(Bandit bandit) { this.bandit = bandit; }

        public override void Attack() { bandit.Attack(); }

        public override int getDamage() { return bandit.getDamage(); }

        public override int getHp() { return bandit.getHp(); }

        public override void setDamage(int dmg) { bandit.setDamage(dmg); }

        public override void setHp(int hp) {
            bandit.loseHp(bandit.getHp() - hp); 
        }
    }
}
