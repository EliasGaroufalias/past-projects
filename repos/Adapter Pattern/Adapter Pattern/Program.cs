﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Goblin goblin = new Goblin();
            Bandit bandit = new Bandit();

            goblin.Attack();
            bandit.Attack();

            Console.ReadKey();
        }
    }
}
