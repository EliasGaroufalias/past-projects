﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Pattern
{
    class Bandit
    {
        int hp;
        int damage;

        public void Attack() { Console.WriteLine("The Bandit swipes with his sword!"); }

        public int getDamage() { return damage; }
        public int getHp() { return hp; }

        public void loseHp(int hp) { this.hp = this.hp - hp; }
        public void setDamage(int dmg) { damage = dmg; }
    }
}
